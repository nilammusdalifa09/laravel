<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sign Up</title>
</head>

<body>
    <form action="/welcome" method="GET">
        @csrf
        <h1>Buat Account Baru!</h1>

        <h3>Sign Up Form</h3>
        <label for="first name">First Name:</label><br><br>
        <input type="text" name="first-name"><br><br>

        <label for="last name">Last Name:</label><br><br>
        <input type="text" name="last-name"><br><br>

        <label for="gender">Gender:</label><br><br>
        <input type="radio" name="gender">Male<br>
        <input type="radio" name="gender">Female<br>
        <input type="radio" name="gender">Other<br><br>

        <label for="nasionality">Nasionality:</label><br><br>
        <select name="nasionality">
            <option value="1">Indonesian</option>
            <option value="2">Singaporean</option>
            <option value="3">European</option>
            <option value="4">African</option>
        </select><br><br>

        <label for="language">Language Spoken:</label><br><br>
        <input type="checkbox" name="language">Bahasa Indonesia<br>
        <input type="checkbox" name="language">English<br>
        <input type="checkbox" name="language">Other<br><br>

        <label for="bio">Bio:</label><br><br>
        <textarea name="bio" cols="30" rows="10"></textarea><br>
        <button>Sign Up</button>
    </form>
</body>

</html>
